package IAM.AuthenticationModule.OrganisationTypes;


import IAM.AuthenticationModule.Entities.OrganisationTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class OrganisationTypeController {

    @Autowired
    private OrganisationTypeRepository orgTypeRepo;

    @GetMapping("organisation-types")
    public List<OrganisationTypes> getAllOrganisationTypes()
    {
        return this.orgTypeRepo.findAll();
    }
}
