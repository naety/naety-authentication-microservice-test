package IAM.AuthenticationModule.OrganisationTypes;

import IAM.AuthenticationModule.Entities.OrganisationTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrganisationTypeRepository extends JpaRepository<OrganisationTypes,Long> {

    @Override
    OrganisationTypes getOne(Long aLong);

    Optional<OrganisationTypes> findByOrganisationTypeId(Long id);
}
