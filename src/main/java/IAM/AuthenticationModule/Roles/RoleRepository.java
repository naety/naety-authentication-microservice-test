package IAM.AuthenticationModule.Roles;

import IAM.AuthenticationModule.Entities.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Roles, Long> {

    Roles findRolesByRoleId(Long roleId);

    @Query(value="select roles.* from roles inner join users on roles.role_id = users.user_role and users.username =:username", nativeQuery = true)
    public Roles findUserRole(@Param("username") String username);

    Roles findRoleByRoleName(String roleName);
}
