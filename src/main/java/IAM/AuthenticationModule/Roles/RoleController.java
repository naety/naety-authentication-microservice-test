package IAM.AuthenticationModule.Roles;

import IAM.AuthenticationModule.Entities.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class RoleController {

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/admin/roles")
    public List<Roles> getAllRoles(){
        return this.roleRepository.findAll();
    }

    @GetMapping("/admin/roles/{id}")
    public  Roles getRole(@RequestParam Long role_id)
    {
        if(role_id != null && role_id >  0)
        {
            return  this.roleRepository.getOne(role_id);
        }
        else {
            return  null;
        }

    }
}
