package IAM.AuthenticationModule.Admin;


import IAM.AuthenticationModule.Entities.CorporateAccount;
import IAM.AuthenticationModule.Entities.Users;
import IAM.AuthenticationModule.Roles.RoleRepository;
import IAM.AuthenticationModule.Users.UserController;
import IAM.AuthenticationModule.Users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.Date;

@RestController
@CrossOrigin(origins = "*")
public class AdminController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserController userController;


    @PostMapping("/register/admin")
    public String createAdmin(@RequestBody AdminRegistrationModel admin)
    {
        Users adminCredentials = new Users(admin.getUsername(), admin.getPassword());

        adminCredentials.setPassword(this.passwordEncoder.encode(admin.getPassword()));


            if(this.userController.createAdmin(adminCredentials) == "created")
            {
                return "admin created";
            }
            else {
                return "failed";
            }
    }
}
