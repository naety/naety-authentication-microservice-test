package IAM.AuthenticationModule.AccountStatus;

import IAM.AuthenticationModule.Entities.AccountStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountStatusRepository extends JpaRepository<AccountStatus, Long> {

    AccountStatus findAccountStatusByStatusId(Long statusId);

    AccountStatus findAccountStatusByStatus(String status);
}
