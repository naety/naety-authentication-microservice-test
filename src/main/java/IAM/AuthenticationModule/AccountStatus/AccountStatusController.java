package IAM.AuthenticationModule.AccountStatus;

import IAM.AuthenticationModule.Entities.AccountStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class AccountStatusController {

    @Autowired
    private AccountStatusRepository accountStatusRepository;

    @GetMapping("/admin/account-status")
    public List<AccountStatus> findAllAccountStatus()
    {
        return this.accountStatusRepository.findAll();
    }
}
