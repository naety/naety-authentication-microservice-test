package IAM.AuthenticationModule.UserAccount;


import IAM.AuthenticationModule.Entities.UserAccount;
import IAM.AuthenticationModule.Entities.Users;
import IAM.AuthenticationModule.Users.UserController;
import IAM.AuthenticationModule.Users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@CrossOrigin
@RestController
public class UserAccountController {

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAccountRepository  userAccountRepository;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    @PostMapping("/register/user")
    public  ResponseEntity<Object> registerUserAccount(@RequestBody UserRegistrationModel user)
    {
        Optional<Users> existingUser = this.userRepository.findByUsername(user.getEmail());
        if(existingUser.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        else {

            Users newLogin = new Users(user.getEmail(), user.getPassword());

            // hash the password with Bcrypt Encoder

            newLogin.setPassword(this.passwordEncoder.encode(newLogin.getPassword()));

            if(this.userController.createUser(newLogin) == "created")
            {
                UserAccount userAccount = new UserAccount(user.getFirstName(),user.getLastName(),user.getEmail(),
                        userRepository.findUsersByUsername(user.getEmail()).getUserRole());

                String dtf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                userAccount.setJoinedDate(new Timestamp(new Date().getTime()));

                userAccountRepository.save(userAccount);
                return new ResponseEntity<>(HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        }

    }


}


