package IAM.AuthenticationModule.UserAccount;

import IAM.AuthenticationModule.Entities.UserAccount;
import IAM.AuthenticationModule.Entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccountRepository extends JpaRepository<UserAccount,Long> {
}
