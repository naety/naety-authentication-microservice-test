package IAM.AuthenticationModule.Security;

import java.util.ArrayList;
import java.util.Collection;

import IAM.AuthenticationModule.Entities.Users;
import IAM.AuthenticationModule.Roles.RoleRepository;
import IAM.AuthenticationModule.Users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = this.userRepository.findUserName(username);
		if(user==null) throw new UsernameNotFoundException(username);

		Collection<GrantedAuthority> autorities=new ArrayList<>();
		System.out.println("username is "+user.getUsername());
		String userRole = roleRepository.findUserRole(user.getUsername()).getRoleName();
		autorities.add(new SimpleGrantedAuthority(userRole));
		return new User(user.getUsername(),user.getPassword(),autorities);
	}

}
