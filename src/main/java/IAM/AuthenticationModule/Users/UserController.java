package IAM.AuthenticationModule.Users;


import IAM.AuthenticationModule.AccountStatus.AccountStatusRepository;
import IAM.AuthenticationModule.Entities.Users;
import IAM.AuthenticationModule.Roles.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountStatusRepository accountStatusRepository;

    @Autowired
    private RoleRepository roleRepository;


    public  String createUser(Users user)
    {
        try {
            user.setUserRole(this.roleRepository.findRoleByRoleName("user").getRoleId());
            user.setAccountStatus(this.accountStatusRepository.findAccountStatusByStatus("inactive").getStatusId());
            this.userRepository.save(user);
            return "created";
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return "failed";
        }
    }


    public String createCorporate(Users corporateCredentials) {

        try {
            corporateCredentials.setUserRole(this.roleRepository.findRoleByRoleName("corporate").getRoleId());

            corporateCredentials.setAccountStatus(this.accountStatusRepository.findAccountStatusByStatus("inactive").getStatusId());
            this.userRepository.save(corporateCredentials);
            return "created";
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return "failed";
        }
    }

    public String createAdmin(Users adminCredentials)
    {
        try
        {
            adminCredentials.setUserRole(this.roleRepository.findRoleByRoleName("ADMIN").getRoleId());
            adminCredentials.setAccountStatus((this.accountStatusRepository.findAccountStatusByStatus("inactive").getStatusId()));
            this.userRepository.save(adminCredentials);
            return  "created";
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return "failed";
        }
    }
}
