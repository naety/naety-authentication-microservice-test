package IAM.AuthenticationModule.Users;

import IAM.AuthenticationModule.Entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

    Users findUsersByUsername(String username);

    Optional<Users> findByUsername(String username);

    @Query(value = "SELECT * FROM users where  username=:username" , nativeQuery = true)
    public Users findUserName(@Param("username") String username);
}
