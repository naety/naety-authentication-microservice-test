package IAM.AuthenticationModule.CorporateAccount;

import java.util.Date;

public class CorporateRegistrationModel {

    private String companyName;
    private String companyEmail;
    private Long organisationType;
    private String password;
    private String phoneNumber;

    public CorporateRegistrationModel() {
    }

    public CorporateRegistrationModel(String companyName, String companyEmail,  Long organisationType, String password , String phoneNumber) {
        this.companyName = companyName;
        this.companyEmail = companyEmail;
        this.organisationType = organisationType;
        this.password = password;
        this.phoneNumber = phoneNumber;
    }


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getOrganisationType() {
        return organisationType;
    }

    public void setOrganisationType(Long organisationType) {
        this.organisationType = organisationType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
