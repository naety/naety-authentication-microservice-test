package IAM.AuthenticationModule.CorporateAccount;

import IAM.AuthenticationModule.Entities.CorporateAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CorporateAccountRepository extends JpaRepository<CorporateAccount , Long> {
}
