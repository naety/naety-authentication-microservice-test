package IAM.AuthenticationModule.CorporateAccount;

import IAM.AuthenticationModule.Entities.CorporateAccount;
import IAM.AuthenticationModule.Entities.OrganisationTypes;
import IAM.AuthenticationModule.Entities.Users;
import IAM.AuthenticationModule.OrganisationTypes.OrganisationTypeRepository;
import IAM.AuthenticationModule.Roles.RoleRepository;
import IAM.AuthenticationModule.Users.UserController;
import IAM.AuthenticationModule.Users.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
public class CorporateAccountController {

    @Autowired
    private CorporateAccountRepository corporateAccountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrganisationTypeRepository orgTypeRepo;

    @Transactional
    @PostMapping("register/corporate")
    public ResponseEntity<Object> createCorporate(@RequestBody CorporateRegistrationModel corporation)
    {
        Optional<Users> existingUser = this.userRepository.findByUsername(corporation.getCompanyEmail());
        Optional<OrganisationTypes> existingOrgType = this.orgTypeRepo.findByOrganisationTypeId(corporation.getOrganisationType());
        if(existingUser.isPresent())
        {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        else if (existingOrgType.isPresent() == false)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        else {

            Users corporateCredentials = new Users(corporation.getCompanyEmail(), corporation.getPassword());

            corporateCredentials.setPassword(this.passwordEncoder.encode(corporation.getPassword()));



            try {
                if(this.userController.createCorporate(corporateCredentials) == "created")
                {
                    try {

                        CorporateAccount newCorporation = new CorporateAccount(corporation.getCompanyName(),corporation.getCompanyEmail(),new Timestamp(new Date().getTime()),
                                corporation.getPhoneNumber());
                        newCorporation.setUserAccount(this.userRepository.findUsersByUsername(corporateCredentials.getUsername()).getUserId());
                        newCorporation.setCorporateType(this.orgTypeRepo.getOne(corporation.getOrganisationType()).getOrganisationTypeId());
                        this.corporateAccountRepository.save(newCorporation);
                        return new ResponseEntity<>(HttpStatus.CREATED);

                    }
                    catch (Exception exception)
                    {
                        exception.printStackTrace();

                        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                    }
                }
                else
                {
                    return new ResponseEntity<>( HttpStatus.BAD_REQUEST);
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        }

    }

}
