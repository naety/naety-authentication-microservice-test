package IAM.AuthenticationModule.Entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Users {
    private Long userId;
    private String username;
    private String password;
    private Long userRole;
    private Long accountStatus;
    private String activationLink;
    private Timestamp activationDate;
    private String passwordRecovery;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 255)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 255)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "user_role", nullable = false)
    public Long getUserRole() {
        return userRole;
    }

    public void setUserRole(Long userRole) {
        this.userRole = userRole;
    }

    @Basic
    @Column(name = "account_status", nullable = false)
    public Long getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Long accountStatus) {
        this.accountStatus = accountStatus;
    }

    @Basic
    @Column(name = "activation_link", nullable = true, length = 255)
    public String getActivationLink() {
        return activationLink;
    }

    public void setActivationLink(String activationLink) {
        this.activationLink = activationLink;
    }

    @Basic
    @Column(name = "activation_date", nullable = true)
    public Timestamp getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Timestamp activationDate) {
        this.activationDate = activationDate;
    }

    @Basic
    @Column(name = "password_recovery", nullable = true, length = 255)
    public String getPasswordRecovery() {
        return passwordRecovery;
    }

    public void setPasswordRecovery(String passwordRecovery) {
        this.passwordRecovery = passwordRecovery;
    }


    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Users() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Users users = (Users) o;

        if (userId != null ? !userId.equals(users.userId) : users.userId != null) return false;
        if (username != null ? !username.equals(users.username) : users.username != null) return false;
        if (password != null ? !password.equals(users.password) : users.password != null) return false;
        if (userRole != null ? !userRole.equals(users.userRole) : users.userRole != null) return false;
        if (accountStatus != null ? !accountStatus.equals(users.accountStatus) : users.accountStatus != null)
            return false;
        if (activationLink != null ? !activationLink.equals(users.activationLink) : users.activationLink != null)
            return false;
        if (activationDate != null ? !activationDate.equals(users.activationDate) : users.activationDate != null)
            return false;
        if (passwordRecovery != null ? !passwordRecovery.equals(users.passwordRecovery) : users.passwordRecovery != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (userRole != null ? userRole.hashCode() : 0);
        result = 31 * result + (accountStatus != null ? accountStatus.hashCode() : 0);
        result = 31 * result + (activationLink != null ? activationLink.hashCode() : 0);
        result = 31 * result + (activationDate != null ? activationDate.hashCode() : 0);
        result = 31 * result + (passwordRecovery != null ? passwordRecovery.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Users{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", userRole=" + userRole +
                ", accountStatus=" + accountStatus +
                ", activationLink='" + activationLink + '\'' +
                ", activationDate=" + activationDate +
                ", passwordRecovery='" + passwordRecovery + '\'' +
                '}';
    }
}
