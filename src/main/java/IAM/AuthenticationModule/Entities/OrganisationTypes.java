package IAM.AuthenticationModule.Entities;

import javax.persistence.*;

@Entity
@Table(name = "organisation_types", schema = "naety", catalog = "")
public class OrganisationTypes {
    private Long organisationTypeId;
    private String organisationTypeName;

    @Id
    @Column(name = "organisation_type_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getOrganisationTypeId() {
        return organisationTypeId;
    }

    public void setOrganisationTypeId(Long organisationTypeId) {
        this.organisationTypeId = organisationTypeId;
    }

    @Basic
    @Column(name = "organisation_type_name", nullable = false, length = 60)
    public String getOrganisationTypeName() {
        return organisationTypeName;
    }

    public void setOrganisationTypeName(String organisationTypeName) {
        this.organisationTypeName = organisationTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganisationTypes that = (OrganisationTypes) o;

        if (organisationTypeId != that.organisationTypeId) return false;
        if (organisationTypeName != null ? !organisationTypeName.equals(that.organisationTypeName) : that.organisationTypeName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (organisationTypeId ^ (organisationTypeId >>> 32));
        result = 31 * result + (organisationTypeName != null ? organisationTypeName.hashCode() : 0);
        return result;
    }
}
