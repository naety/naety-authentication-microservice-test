package IAM.AuthenticationModule.Entities;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "corporate_account", schema = "naety", catalog = "")

public class CorporateAccount {
    private Long corporateId;
    private String corporateName;
    private String corporateEmail;
    private String corporateWebsite;
    private Timestamp registrationDate;
    private Long country;
    private Long city;
    private String numberOfEmployees;
    private String registrationNumber;
    private String description;
    private String phoneNumber;
    private String coverPhoto;
    private Long domainOfActivity;
    private String slogan;
    private String tagline;
    private Long corporateType;
    private Long industryType;
    private Date yearFounded;
    private Long userAccount;

    public CorporateAccount() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "corporate_id", nullable = false)
    public Long getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(Long corporateId) {
        this.corporateId = corporateId;
    }

    @Basic
    @Column(name = "corporate_name", nullable = true, length = 100)
    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    @Basic
    @Column(name = "phoneNumber", nullable = false, length = 100)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "corporate_email", nullable = true, length = 100)
    public String getCorporateEmail() {
        return corporateEmail;
    }

    public void setCorporateEmail(String corporateEmail) {
        this.corporateEmail = corporateEmail;
    }

    @Basic
    @Column(name = "corporate_website", nullable = true, length = 100)
    public String getCorporateWebsite() {
        return corporateWebsite;
    }

    public void setCorporateWebsite(String corporateWebsite) {
        this.corporateWebsite = corporateWebsite;
    }

    @Basic
    @Column(name = "registration_date", nullable = true)
    public Timestamp getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Timestamp registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Basic
    @Column(name = "country", nullable = true)
    public Long getCountry() {
        return country;
    }

    public void setCountry(Long country) {
        this.country = country;
    }

    @Basic
    @Column(name = "city", nullable = true)
    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    @Basic
    @Column(name = "number_of_employees", nullable = true, length = 20)
    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    @Basic
    @Column(name = "registration_number", nullable = true, length = 255)
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Basic
    @Column(name = "description", nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "cover_photo", nullable = true, length = 255)
    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    @Basic
    @Column(name = "domain_of_activity", nullable = true)
    public Long getDomainOfActivity() {
        return domainOfActivity;
    }

    public void setDomainOfActivity(Long domainOfActivity) {
        this.domainOfActivity = domainOfActivity;
    }

    @Basic
    @Column(name = "slogan", nullable = true)
    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    @Basic
    @Column(name = "tagline", nullable = true, length = 255)
    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    @Basic
    @Column(name = "corporate_type", nullable = true)
    public Long getCorporateType() {
        return corporateType;
    }

    public void setCorporateType(Long corporateType) {
        this.corporateType = corporateType;
    }

    @Basic
    @Column(name = "industry_type", nullable = true)
    public Long getIndustryType() {
        return industryType;
    }

    public void setIndustryType(Long industryType) {
        this.industryType = industryType;
    }

    @Basic
    @Column(name = "year_founded", nullable = true)
    public Date getYearFounded() {
        return yearFounded;
    }

    public void setYearFounded(Date yearFounded) {
        this.yearFounded = yearFounded;
    }

    @Basic
    @Column(name = "user_account", nullable = false)
    public Long getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(Long userAccount) {
        this.userAccount = userAccount;
    }

    public CorporateAccount(String corporateName, String corporateEmail, Timestamp registrationDate,  String phoneNumber) {
        this.corporateName = corporateName;
        this.corporateEmail = corporateEmail;
        this.registrationDate = registrationDate;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CorporateAccount that = (CorporateAccount) o;

        if (corporateId != null ? !corporateId.equals(that.corporateId) : that.corporateId != null) return false;
        if (corporateName != null ? !corporateName.equals(that.corporateName) : that.corporateName != null)
            return false;
        if (corporateEmail != null ? !corporateEmail.equals(that.corporateEmail) : that.corporateEmail != null)
            return false;
        if (corporateWebsite != null ? !corporateWebsite.equals(that.corporateWebsite) : that.corporateWebsite != null)
            return false;
        if (registrationDate != null ? !registrationDate.equals(that.registrationDate) : that.registrationDate != null)
            return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (numberOfEmployees != null ? !numberOfEmployees.equals(that.numberOfEmployees) : that.numberOfEmployees != null)
            return false;
        if (registrationNumber != null ? !registrationNumber.equals(that.registrationNumber) : that.registrationNumber != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (coverPhoto != null ? !coverPhoto.equals(that.coverPhoto) : that.coverPhoto != null) return false;
        if (domainOfActivity != null ? !domainOfActivity.equals(that.domainOfActivity) : that.domainOfActivity != null)
            return false;
        if (slogan != null ? !slogan.equals(that.slogan) : that.slogan != null) return false;
        if (tagline != null ? !tagline.equals(that.tagline) : that.tagline != null) return false;
        if (corporateType != null ? !corporateType.equals(that.corporateType) : that.corporateType != null)
            return false;
        if (industryType != null ? !industryType.equals(that.industryType) : that.industryType != null) return false;
        if (yearFounded != null ? !yearFounded.equals(that.yearFounded) : that.yearFounded != null) return false;
        if (userAccount != null ? !userAccount.equals(that.userAccount) : that.userAccount != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = corporateId != null ? corporateId.hashCode() : 0;
        result = 31 * result + (corporateName != null ? corporateName.hashCode() : 0);
        result = 31 * result + (corporateEmail != null ? corporateEmail.hashCode() : 0);
        result = 31 * result + (corporateWebsite != null ? corporateWebsite.hashCode() : 0);
        result = 31 * result + (registrationDate != null ? registrationDate.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (numberOfEmployees != null ? numberOfEmployees.hashCode() : 0);
        result = 31 * result + (registrationNumber != null ? registrationNumber.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (coverPhoto != null ? coverPhoto.hashCode() : 0);
        result = 31 * result + (domainOfActivity != null ? domainOfActivity.hashCode() : 0);
        result = 31 * result + (slogan != null ? slogan.hashCode() : 0);
        result = 31 * result + (tagline != null ? tagline.hashCode() : 0);
        result = 31 * result + (corporateType != null ? corporateType.hashCode() : 0);
        result = 31 * result + (industryType != null ? industryType.hashCode() : 0);
        result = 31 * result + (yearFounded != null ? yearFounded.hashCode() : 0);
        result = 31 * result + (userAccount != null ? userAccount.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        return result;
    }
}
