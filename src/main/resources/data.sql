INSERT into roles (role_name) values
("USER"),
("CORPORATE"),
("ADMIN");

INSERT INTO organisation_types (organisation_type_name) VALUES
("Charity"),
("Company"),
("University"),
("Religious Organization");

insert into account_status (status) values
("inactive"),
("active"),
("blocked");